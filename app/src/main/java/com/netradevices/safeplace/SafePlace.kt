package com.netradevices.safeplace

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat.startActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnSuccessListener
import kotlinx.android.synthetic.main.activity_settings.*

class SafePlace : AppCompatActivity(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
                  GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {


    //Google Map related Variables
    private lateinit var mMap: GoogleMap
    private var newLat: Double? = null
    private var newLang: Double? = null
    private var AlarmLat: Double? = null
    private var AlarmLong: Double? = null
    private var UserLat: Double? = null
    private var UserLong: Double? = null
    //location variables
    private val TAG = "SafePlace"
    //Location Manager class provides access to the system location services. These services allow applications to obtain
    // periodic updates of the devices's geographical location, or to fire an application-specified Intent when the device
    // enters the proximity of a given geographical location.
    private lateinit  var mLocationManager: LocationManager
    lateinit var mLocation: Location

    // A data object that contains quality of service parameters for requests to the FusedLocationProviderApi
    private var mLocationRequest: LocationRequest? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private lateinit var mLocationCallback: LocationCallback
    private lateinit var mLocationResult: LocationResult



    // Google Play API related variables
    //Update intervals in which we would like to get updates from the Google API
    //10 seconds rate at which we would like to get the update
    private lateinit var mGoogleApiClient: GoogleApiClient
    private val UPDATE_INTERVAL = 10000.toLong()
    //5 seconds rate at which App can handle the updates
    private val FASTEST_INTERVAL: Long = 500


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_safe_place)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.CheckPlace) as SupportMapFragment
        mapFragment.getMapAsync(this)

        //Google API client provides a common entry point to Google play services and manages the
        // network connections, between the user's device and each google service.
        // this is however depreciated
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener (this)
            .addApi(LocationServices.API)
            .build()

        mLocationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        checkGPS()
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(mlocationResult: LocationResult?) {
                super.onLocationResult(mlocationResult)
                mlocationResult ?: return
                for (location in mlocationResult.locations){
                    Log.i(TAG, "Latitude = "+location.latitude+" Longitutde ="+location.longitude)
                }
            }
        }

    }

    /*---------------------GOOGLE API Function Overrides : Start-------------------------------*/
    // This function is called when Activity is becoming visible to the user
    override fun onStart() {
        super.onStart()
        if (!mGoogleApiClient.isConnected) {
            mGoogleApiClient.connect()
        }
        toast("onStart")
    }

    // This function is called when activity is no longer visible to the user
    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient.isConnected) {
            mGoogleApiClient.disconnect()
        }
        toast("onStop")
    }


    //Handle if connection request fails
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.i(TAG, "Connection failed: Error" + connectionResult.errorMessage)
        toast("Connection failed: Error" + connectionResult.errorMessage)
    }

    private fun toast(msg: String){
        Toast.makeText(this,msg, Toast.LENGTH_LONG).show()

    }
    //Try connection if Connection gets suspended
    override fun onConnectionSuspended(p0: Int) {
        Log.i(TAG, "Connection Suspended")
        toast("Connection Suspended")
        mGoogleApiClient.connect()
    }

    override fun onConnected(p0: Bundle?) {

           toast("Connected on")
            startLocationUpdates()
            //FusedLocationProviderClient gives the current location details

            if ((ActivityCompat.checkSelfPermission
                    (this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                (ActivityCompat.checkSelfPermission
                    (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            ) {


                fusedLocationProviderClient.lastLocation
                    .addOnSuccessListener({ location: Location? ->
                        if (location != null) mLocation = location

                    })
            }
        else
                toast("permissions not there for onconnected")

    }

    override fun onPause() {
        super.onPause()
        toast("onPause")
        stopLocationUpdates()
    }

    private fun stopLocationUpdates() {
        toast("stop location updates")
        fusedLocationProviderClient.removeLocationUpdates(mLocationCallback)
    }

    override fun onResume() {
        super.onResume()
        toast("resume")
        startLocationUpdates()
    }

    /*---------------------GOOGLE API Function Overrides : Ends -------------------------------*/


    private fun startLocationUpdates() {
        // Create the location request
        toast("start location updates")
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(UPDATE_INTERVAL)
            .setFastestInterval(FASTEST_INTERVAL)

        // Request locaiton updates
        // First check if permissions are granted
        if ((ActivityCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
            (ActivityCompat.checkSelfPermission
                (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        ) fusedLocationProviderClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback, Looper.myLooper())
        else
            toast("permission not there for location updates")
    }

    //Below are the mandatory overrides to be implemented

    override fun onLocationChanged(location: Location) {
        var msg = "Updated Location: " + location.latitude  + " , " +location.longitude

        // show toast message with updated location
        Toast.makeText(this,msg, Toast.LENGTH_LONG).show()
        val loc = LatLng(location.latitude, location.longitude)
        mMap.clear()
        mMap.addMarker(MarkerOptions().position(loc).title("current position"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc))


    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera

        val kundalahalli = LatLng(2.9668479,77.7130428)

        mMap.addMarker(MarkerOptions().position(kundalahalli).title("Marker at Home"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(kundalahalli))
    }

    fun onClickSettingsButton(view: View){
        val intent = Intent("android.intent.action.SETTINGACTIVITY")
        startActivity(intent)
    }

    private fun checkGPS() : Boolean {
        if (!isLocationEnabled()) {
            Toast.makeText(this,"Enable GPS location!!!",Toast.LENGTH_LONG).show()
        }
        else
            toast("GPS is enabled !!!")
        return isLocationEnabled()
    }
    private fun isLocationEnabled(): Boolean {
         val status : Boolean = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
         return status
    }
} //ends the main activity class safeClass

class Settings : AppCompatActivity() {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
    }

    fun onOKClicked (view: View){

        //Two ways to extract the EditText values
        //Way 1:
        //val latitude = findViewById(R.id.Latitude) as EditText
        //val lat = lat.getText().toString()

        //Way2:
        var lat = this.Latitude.text.toString()

        //Similar way extract Longitutde
        var lang = this.Longitude.text.toString()

        //Put Latitude and Longitude in a string to be displayed a message
        var displayString = "Alarm Set for: "+ lat +", "+lang

        Toast.makeText(this,displayString,Toast.LENGTH_LONG).show()

        //Save the user provided locations in a local shared file
        val sharedFile = this.getSharedPreferences(getString(R.string.PREFS_NAME), Context.MODE_PRIVATE) ?: return

        //Write to the file
        val editor = sharedFile.edit()
            editor.putString("userLat",lat)
            editor.putString("userLang",lang)
            editor.commit()


        Toast.makeText(this,"file saved!!!",Toast.LENGTH_LONG).show()

        //Read from the file
        // Check if data is already present in the shared preferences

        val sharedPref = this.getSharedPreferences(getString(R.string.PREFS_NAME),Context.MODE_PRIVATE)
        if (sharedPref.contains("userLat")) {
            lang = sharedFile.getString("userLang","")
            lat = sharedFile.getString("userLat","")
            displayString = "Saved data was $lang, $lat"
            Toast.makeText(this, displayString,Toast.LENGTH_LONG).show()
        }

        //convert the read lang and lat to float
        val latAlarm = lat.toDoubleOrNull()
        val langAlarm = lang.toDoubleOrNull()

        Log.i("IFNO","Double values = lat: $latAlarm, lang: $langAlarm")

    }

}
